<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class ClassModel extends Authenticatable
{
    //
    protected $table = 'classes';
}
