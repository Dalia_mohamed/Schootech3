<?php

namespace App\Http\Controllers\student;

use App\Http\Controllers\Controller;
use App\Models\Users\student;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class StudentController extends Controller
{
    protected $student_info;
    protected $courses;
    
  // public function __construct()
  //   {
  //       $this->middleware('auth:student');
  //   }

    protected function index()
    {
    	// $id =1;  //get it rom Login letar
      $id = Auth::id();
    	$student_info= student::find($id);
      $a= $student_info->class_id;  //a= student's class id
      
      $courses_info = DB::table('teach')
                          ->join('courses', 'teach.course_id', '=', 'courses.id')
                          ->where(function ($query) use ($a) {
                            $query->where('class_id','=', $a);
                            })->get();

      session(['student_info' => $student_info]);  

      return view('student/student',compact('a','student_info','courses_info'));
    }

}
