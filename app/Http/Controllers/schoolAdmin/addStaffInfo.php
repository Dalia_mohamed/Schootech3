<?php


namespace App\Http\Controllers\schoolAdmin;
use App\Http\Controllers\Controller;
use App\staff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use View;

class addStaffInfo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Add_School_info.add_staff_info');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('Add_School_info.add_staff_info');    }


 public function __construct()
    {
        $this->middleware('auth:admin');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $staff=new staff;
          $this->validate($request,[
            "name"=>'required|string|max:50',
            'email' => 'required|string|email|max:255|unique:staff',
            'password' => 'required|string|min:6',
            'inputPhone' => 'required|regex:/[0-9]/|max:11|min:8',
            'inputAddress' => 'required|string|min:3',
        ]);

        $staff ->name = $request ->name;
        $staff ->phone = $request ->inputPhone ;
        $staff ->email = $request ->email ;
        $staff ->password = Hash::make($request->input('password'));
        $staff ->address = $request ->inputAddress;
        $staff ->school_id=session('school_id');
        $staff -> save();
        return redirect('Add_School_Info/add_staff_info');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
