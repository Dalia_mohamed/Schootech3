<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
class school extends Authenticatable
{
 
    protected $fillable = [
        'name', 'phone', 'address','vision','mission',
    ];
}
