@extends('layouts.SAdminHeader')

@section('title')
Add school's info
@endsection
@section('css')
  <link rel="stylesheet" href="../css/add_school_info.css">

@endsection

@section('content')

<div class="container">


<form id="SchoolInfo" method= "post" action="{{ route('add_School_info.store') }}">
        @csrf
<div class="form-group col-md-6">
<label for="SInfo">{{ __('School Info') }}</label>
</div>

  <div class="form-row">
    <div class="form-group col-md-6">
      <input type="text" id="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus  placeholder="Name">

       @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
    </div>

    <div class="form-group col-md-6">
      <input type="text" class="form-control{{ $errors->has('inputPhone') ? ' is-invalid' : '' }}" name="inputPhone" value="{{ old('inputPhone') }}" id="inputhone" placeholder="Phone" >

       @if ($errors->has('inputPhone'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('inputPhone') }}</strong>
                                    </span>
                                @endif
    </div>
  </div>


  <div class="form-group ">
    <input type="text" class="form-control{{ $errors->has('inputAddress') ? ' is-invalid' : '' }}" name="inputAddress" value="{{ old('inputAddress') }}"" id="inputAddress" placeholder="Address">

     @if ($errors->has('inputAddress'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('inputAddress') }}</strong>
                                    </span>
                                @endif
    </div>
  


  <div class="form-group ">
    <textarea class="form-control" id="inputVision" name="inputVision" placeholder="Vision" required></textarea> 
  </div>

  <div class="form-group ">
    <textarea class="form-control" id="inputMission" name="inputMission" placeholder="Mission" required></textarea> 
  </div>
    <button type="submit" class="btn btn-primary" >{{ __('Submit') }}</button>
</form>
</div>

@endsection



{{-- @extends('layout.header') --}}





   
