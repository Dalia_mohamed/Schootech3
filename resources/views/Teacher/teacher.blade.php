@extends('layout.header')
@extends('layout.footer')
@section('title','| Teacher')
@section('css')
{{'../css/teacher.css'}}
@endsection

<div class="teacher">
  <div class="overlay">
    <div class="container row wow slideInLeft" style="margin:auto; padding-top:90px;">

      <div class="col-lg-4">
      <a href='teacher/schedule'>
        <div class="TimeTable">
            <div class="overlay ">
              <p class="text-center">Show TimeTable</p>
            </div> 
        </div>
      </a>
      </div>

      <div class="col-lg-4">
      <a href='AddEx.php'>
        <div class="Exercise">
          <div class="overlay">
            <p class="text-center">Add Exercise</p>
          </div> 
        </div>
      </a>
      </div>

      <div class="col-lg-4">
      <a href='AddGrades.php'>
        <div class="Grades">
          <div class="overlay">
            <p class="text-center" >Add Grades</p>
          </div> 
        </div>
      </a>
      </div>

    </div>
  </div>
</div>








<?php 
    // $js_files = '<script src="../js/about.js"></script>';

    // include  '../' . $tmpl . 'footer.php';

    ?>   
