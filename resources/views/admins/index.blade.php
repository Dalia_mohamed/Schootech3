@extends('layouts.header')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card text-center" style="margin-top: 112px;height: 220px;
    margin-bottom: 30px; ">
                <div class="card-header text-center">Welcome</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif  
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                       You are logged in successfully!
                      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <button class="btn btn-outline-info mt-5"> <a href=" {{ route('add_admin.index') }} " > Add Admin</a></button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
