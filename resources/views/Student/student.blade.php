@php

  $student=$student_info;

@endphp

@extends('layouts.header')


@section('title')
{{ $student->name }}
@endsection

@section('css')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,900i">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Comfortaa:400,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('../css/student.css') }}">
@endsection
   

@section('content')  {{-- body --}}
  <div class="container" style="padding-top: 100px"><!-- style="position:relative;top:100px"-->
  <!--start Courses sec-->
    <div class="flex-container">
      <div class="row">
        <div class="col-lg-12">
          <h1 class="subheader">My Courses</h1>
        </div>
        @php  $i=0;  @endphp
        @foreach ($courses_info as $course) 
           <div class="col-md-6 col-lg-4">
               <div class="card" style="width: 18rem;">
                  <img class="card-img-top img-responsive" src="../images/student/ma.jpg" alt="Card image cap">
                  <div class="card-body">
                    <center><h3><?php echo $course->name ?></h3></center>
                    <a href="{{ route('course',compact('i')) }}"class="btn card-btn">View Course</a>
                  </div>
                </div>
           </div>
           @php   $i++; @endphp
        @endforeach 
      </div>
    </div>
    <!--end Courses sec-->
    <!--Start schedule sec-->
    <div class="col-lg-12">
      <div bgcolor="skyblue">
        <h1 class="subheader"><font color="DARKCYAN">'Class schedule'</font></h1>
        <div class="schedule">
          <table border="2" cellspacing="3" align="center" style="margin-top:20px;margin-bottom:35px;border-radius:30px;">
            <tr>
                 <td align="center">
                 <td>8:30-9:30
                 <td>9:30-10:30
                 <td>10:3-11:30
                 <td>11:30-12:30
                 <td>12:30-2:00
                 <td>2:00-3:00
                 <td>3:00-4:00
                 <td>4:00-5:00
            </tr>
            <tr>
                <td align="center">SUNDAY</td>
                <td align="center">---</td><td align="center"><font color="blue">SUB1</font><br></td>
                <td align="center"><font color="pink">SUB2</font><br></td>
                <td align="center"><font color="red">SUB3</font><br></td>
                 <td rowspan="6"align="center">B<br>R<br>E<br>A<br>K</td>
                <td align="center"><font color="maroon">SUB4</font><br></td>
                <td align="center"><font color="brown">SUB5</font><br></td>
                <td align="center">counselling class</td>
            </tr>
            <tr>
                <td align="center">MONDAY</td>
                <td align="center">---</td><td align="center"><font color="blue">SUB1</font><br></td>
                <td align="center"><font color="pink">SUB2</font><br></td>
                <td align="center"><font color="red">SUB3</font><br></td>
                <td align="center"><font color="maroon">SUB4</font><br></td>
                <td align="center"><font color="brown">SUB5</font><br></td>
                <td align="center">counselling class</td>
            </tr>
            <tr>
                <td align="center">TUESDAY</td>
                <td align="center"><font color="blue">SUB1</font><br></td>
                <td align="center"><font color="red">SUB2</font><br></td>
                <td align="center"><font color="pink">SUB3</font><br></td>
                <td align="center">---</td>
                <td align="center"><font color="orange">SUB4</font><br></td>
                <td align="center"><font color="maroon">SUB5</font><br></td>
                <td align="center">library</td>
            </tr>
            <tr>
                <td align="center">WEDNESDAY</td>
                <td align="center"><font color="pink">SUB1</font><br></td>
                <td align="center"><font color="orange">SUB2</font><br></td>
                <td align="center"><font color="brown">SWA</font><br></td>
                <td align="center">---</td>
                <td colspan="3" align="center"><font color="green"> lab</font></td>
            </tr>
            <tr>
                <td align="center">THURSDAY
                <td align="center">SUB1<br>
                <td align="center"><font color="brown">SUB2</font><br>
                <td align="center"><font color="orange">SUB3</font><br>
                <td align="center">---
                <td align="center"><font color="blue">SUB4</font><br>
                <td align="center"><font color="red">SUB5</font><br>
                <td align="center">library
            </tr>
            <tr>
                <td align="center">FRIDAY
                <td align="center"><font color="orange">SUB1</font><br>
                <td align="center"><font color="maroon">SUB2</font><br>
                <td align="center"><font color="blue">SUB3</font><br>
                <td align="center">---
                <td align="center"><font color="pink">SUB4</font><br>
                <td align="center"><font color="brown">SUB5</font><br>
                <td align="center">library
            </tr>
          </table>
        </div>
      </div>
    </div>
    <!-- end schedule sec -->
      
<!--Start Grades sec-->
    <div class="col-lg-12">
        <h1 class="subheader">My Grades</h1>
    </div>
    <table class="table" style="margin-top:20px;margin-bottom:35px;">
      <thead class="thead-dark">
        <tr>
          <th scope="col">#</th>
          <th scope="col">Subject</th>
          <th scope="col">Grade</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">1</th>
          <td>Arabic</td>
          <td>20</td>
        </tr>
        <tr>
          <th scope="row">2</th>
          <td>English</td>
          <td>18</td>
        </tr>
        <tr>
          <th scope="row">3</th>
          <td>History</td>
          <td>20</td>
        </tr>
      </tbody>
    </table>
    <!--End Grades sec-->
   
    <!--Start Calender sec-->
    <div class="calender-body" onload="Day()">
      <div class="calender">
          <div id="day"></div>
          <div id="date"></div>

        </div>
    </div>
      <!--End Calender sec -->
  </div>
@endsection




@section('js')
<link rel="stylesheet" href="../js/student.js">
@endsection
