@extends('layouts.header')

@section('css')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,900i">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Comfortaa:400,700" rel="stylesheet">
<link rel="stylesheet" href="{{ asset('../css/student.css') }}">
@endsection

    <!--start Events sec-->
    <div class="container " style="padding-top: 120px;">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="subheader" style="margin-bottom:25px;">School Events</h1>
            </div>
            <div class="row event">
              <div class="col-lg-8 col-md-12">
                  <p class="event-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                  Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
              </div>
              <div class="col-lg-4 col-md-12">
                 <!--<div class="event_photo">-->
                       <img class="event-photo" src="../images/student/sport.jpg">
                  <!--</div>-->
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="row event">
              <div class="col-lg-4 col-md-12">
                      <img class="event-photo" src="../images/student/science.jpg">
              </div>
              <div class="col-lg-8 col-md-12">
                  <p class="event-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
              </div>
          </div>
            <div class="clearfix"></div>
            <div class="row event">
                <div class="col-lg-8 col-md-12">
                  <p class="event-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat</p>
              </div>
              <div class="col-lg-4 col-md-12">
                 <!--<div class="event_photo">-->
                       <img class="event-photo" src="../images/student/football.jpg">
                  <!--</div>-->
              </div>
          </div>
        </div>
        <div class="clearfix"></div>
    </div>
 <!--end Events sec-->